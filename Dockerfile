# ENIGMA1/2 BBS Dockerfile

FROM alpine:3.14

MAINTAINER DuckHuntPr0
LABEL maintainer="DuckHuntPr0" version="-dev" description="ENIGMA1/2 BBS Docker"

ARG USER_ID
ARG GROUP_ID

## Required
ENV REQUIRED_PACKS shadow curl wget git nano less bash zip unzip supervisor tmux make g++ python3 nodejs-current npm xxhash linux-headers openssl ca-certificates libstdc++

# ca-certificates and libstdc++ are required for glibc (to run 'sexyz')
ARG APK_GLIBC_VERSION=2.33-r0
ARG APK_GLIBC_FILE="glibc-${APK_GLIBC_VERSION}.apk"
ARG APK_GLIBC_BIN_FILE="glibc-bin-${APK_GLIBC_VERSION}.apk"
ARG APK_GLIBC_BASE_URL="https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${APK_GLIBC_VERSION}"



#build-base make autoconf perl python2 nspr-dev notcurses-dev libarchive-dev gcompat

#useful archive/compression tools
ENV UTILS_ARCHIVE lzip bzip2 xz gzip tar p7zip archivemount advancecomp lha unrar lz4 lzo

ENV URL_NVM_INSTALL_SCRIPT "https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh"
ENV URL_ENIGMA_GIT_REPO "https://github.com/NuSkooler/enigma-bbs.git"


USER root
RUN	echo -e "\033[1;32m########################################\n# Preparing base system..\n########################################\033[0m"  \
	&& apk update --progress --no-cache \
	&& apk upgrade --progress --no-cache \
	&& apk add --progress --no-cache ${REQUIRED_PACKS} ${UTILS_ARCHIVE} \
	&& npm update -g \
	&& echo "Getting Synchronet BBS' sexyz binary ( see: http://wiki.synchro.net/util:sexyz?s[]=sexyz )" \
	&& apk add --progress --no-cache libklibc --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/ \
	&& curl --connect-timeout 60 --max-time 160 --retry 5 --retry-delay 10 --retry-max-time 600 \
		--output - 'ftp://ftp.synchro.net/sbbs317b.tgz' | gunzip | tar xvf - -C /tmp/ sexyz \
	&& mv /tmp/sexyz /usr/bin


# Get and install glibc for alpine
USER root
RUN	wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
	&& wget "${APK_GLIBC_BASE_URL}/${APK_GLIBC_FILE}" \
	&& apk --no-cache add "${APK_GLIBC_FILE}" \
	&& wget "${APK_GLIBC_BASE_URL}/${APK_GLIBC_BIN_FILE}" \
	&& apk --no-cache add "${APK_GLIBC_BIN_FILE}" \
	&& rm glibc-*


RUN	echo -e "\033[1;32m########################################\n# Creating user 'bbs' and group 'bbs'..\n########################################\033[0m"  \
	&& adduser -h /home/bbs -D -s /bin/bash bbs \
	&& groupmod -g ${GROUP_ID} bbs \
	&& usermod -u ${USER_ID} -g ${GROUP_ID} bbs \
	&& chown -R bbs:bbs /home/bbs \
	&& su bbs -c "cd /home/bbs && touch ~/.bashrc" \
	&& su bbs -c "cd /home/bbs && touch ~/.profile"

RUN 	mkdir /home/bbs/enigma-bbs \
	&& chown -R bbs:bbs /home/bbs/enigma-bbs


USER bbs
RUN	echo -e "\033[1;32m########################################\n# Creating '/home/bbs/.install_enigma.sh' script..\n########################################\033[0m" \
	&& touch ~/.install_enigma.sh \
	&& chmod +x ~/.install_enigma.sh \
	&& echo -e "#!/usr/bin/env bash" >> ~/.install_enigma.sh \
	&& echo -e "cd ~/" >> ~/.install_enigma.sh \
	&& echo -e "echo \"Installing Enigma BBS...\"" >> ~/.install_enigma.sh \ 
	&& echo -e "curl -o- \$URL_NVM_INSTALL_SCRIPT | bash" >> ~/.install_enigma.sh \
	&& echo -e "export NVM_DIR=\"\$HOME/.nvm\"" >> ~/.install_enigma.sh \
	&& echo -e "[ -s \"\$NVM_DIR/nvm.sh\" ] && \. \"\$NVM_DIR/nvm.sh\"" >> ~/.install_enigma.sh \
	&& echo -e "[ -s \"\$NVM_DIR/bash_completion\" ] && \. \"\$NVM_DIR/bash_completion\"" >> ~/.install_enigma.sh \
	&& echo -e "cd ~/enigma-bbs" >> ~/.install_enigma.sh \
	&& echo -e "git init" >> ~/.install_enigma.sh \
	&& echo -e "git remote add origin \$URL_ENIGMA_GIT_REPO" >> ~/.install_enigma.sh \
	&& echo -e "git fetch --depth=1 origin" >> ~/.install_enigma.sh \
	&& echo -e "git checkout -b master --track origin/master" >> ~/.install_enigma.sh \
	&& echo -e "npm install" >> ~/.install_enigma.sh \
	&& echo -e "npm audit fix" >> ~/.install_enigma.sh \
	&& echo -e "echo -e \"\n\n\033[1;33m########################################\n\"" >> ~/.install_enigma.sh \
	&& echo -e "echo -e \"\033[1;33m########### !! ATTENTION !! ############\n\"" >> ~/.install_enigma.sh \
	&& echo -e "echo -e \"\033[1;33m########################################\033[0m\"" >> ~/.install_enigma.sh \
	&& echo -e "printf '\a' && echo -n '\a'" >> ~/.install_enigma.sh \
	&& echo -e "echo -e \"\nReady to run Enigma's configuration generator (./oputil.js config new) ..\"" >> ~/.install_enigma.sh \
	&& echo -e "echo -e \"(Now might be a good time to have a look at https://nuskooler.github.io/enigma-bbs/)\"" >> ~/.install_enigma.sh \
	&& echo -e "read -p \"Press any key to start... \" -n1 -s" >> ~/.install_enigma.sh \ 
	&& echo -e "echo -e \"\033[0m\n\n\"" >> ~/.install_enigma.sh \
	&& echo -e "./oputil.js config new" >> ~/.install_enigma.sh \
	&& echo -e "echo -e \"\nReady to generate Enigma's SSH server key(s) 'enigma-bbs/config/security/ssh_private_key.pem' ..\"" >> ~/.install_enigma.sh \
	&& echo -e "mkdir ./config/security" >> ~/.install_enigma.sh \
	&& echo -e "openssl genrsa -aes128 -out ./config/security/ssh_private_key.pem 2048" >> ~/.install_enigma.sh \
	&& echo -e "echo -e \"\033[0m\n\n\"" >> ~/.install_enigma.sh \
	&& echo -e "echo -e \"\033[1;33mNow please review and edit your generated 'enigma-bbs/config/config.hjson' before running 'run.sh' again.\033[0m\n\"" >> ~/.install_enigma.sh


USER bbs
RUN	echo -e "\033[1;32m########################################\n# Creating '/home/bbs/.start_enigma.sh' script..\n########################################\033[0m" \
	&& touch ~/.start_enigma.sh \
	&& chmod +x ~/.start_enigma.sh \
	&& echo -e "#!/usr/bin/env bash" >> ~/.start_enigma.sh \
	&& echo -e "cd ~/" >> ~/.start_enigma.sh \
	&& echo -e "echo \"Starting Enigma BBS...\"" >> ~/.start_enigma.sh \
	&& echo -e "cd ~/enigma-bbs/" >> ~/.start_enigma.sh \
	&& echo -e "./main.js" >> ~/.start_enigma.sh


USER root
RUN	echo -e "\033[1;32m########################################\n# Creating '/.boot.sh' script..\n########################################\033[0m"  \
	&& touch /.boot.sh \
	&& chmod +x /.boot.sh \
	&& echo -e "#!/usr/bin/env bash\n" >> /.boot.sh \
	&& echo -e "echo \"Booting...\"\n" >> /.boot.sh \
	&& echo -e "if [ ! -f /home/bbs/enigma-bbs/config/config.hjson ]" >> /.boot.sh \
	&& echo -e "then" >> /.boot.sh \
	&& echo -e "\techo \"Enigma BBS doesnt Exists. Building and installing now..\"" >> /.boot.sh \
	&& echo -e "\tchown -R bbs:bbs /home/bbs/enigma-bbs" >> /.boot.sh \
	&& echo -e "\tsu bbs -c \"/bin/bash /home/bbs/.install_enigma.sh\"" >> /.boot.sh \
	&& echo -e "else" >> /.boot.sh \
	&& echo -e "\techo \"Existing Enigma BBS install found..\"" >> /.boot.sh \
	&& echo -e "\tsu bbs -c \"/bin/bash /home/bbs/.start_enigma.sh\"" >> /.boot.sh \
	&& echo -e "fi" >> /.boot.sh


# Enigma telnet port
EXPOSE 8888/tcp
# Enigma SSH port
EXPOSE 8889/tcp
# Enigma webSocket ports
EXPOSE 8810/tcp
EXPOSE 8811/tcp
# Enigma http ports
EXPOSE 8080/tcp
EXPOSE 8443/tcp
# Enigma gopher port
EXPOSE 8070/tcp
# Enigma nntp port
EXPOSE 8119/tcp
EXPOSE 8563/tcp
# Enigma MRC port
EXPOSE 5000


ENTRYPOINT ["/.boot.sh"]


