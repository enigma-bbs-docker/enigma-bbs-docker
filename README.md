# Enigma½ BBS docker container

**This is an _unofficial_ Enigma½ BBS docker container. It's author is not affiliated with the Enigma½ BBS development in any way.**

## How to build and run it

```
./build.sh
```

This will build the base container system, and create the boot- and the start- scripts necessary.

```	
./run.sh
```

This will, in case of it being the first time it's being ran, build and install Enigma½ BBS. It will also interactively ask you to do basic configuration.

However, if an already present install of Enigma½ BBS is found, it should simply go ahead and start that up (running `'pm2-runtime main.js'`)

## How to connect to it

Once it's up and running, you should be able to connect to it with:

```
telnet localhost 8888
```

Though, using a more capable telnet/ssh client such as e.g _synchroterm_ or _qodem_ is recommended.

## Customizing and configuration

The Enigma½ BBS installation should be available in the build location under the folder `enigma-bbs`.

This folder is also what gets mounted and used as a docker container volume. (Mounted at `/home/bbs/enigma-bbs` in the container).

### Launching a shell on your running Enigma container

See the ```shell_in.sh``` script. But basically:

```
docker ps
```

To obtain the correct container id.

Then, run:

```
./shell_in.sh <container id>
```

Which should provide you a root shell on your running Enigma BBS container.

## Links

- https://nuskooler.github.io/enigma-bbs
- https://enigma-bbs.github.io/
- https://github.com/NuSkooler/enigma-bbs
- https://github.com/Unitech/pm2