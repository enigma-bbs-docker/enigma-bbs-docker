#!/usr/bin/env bash

# Build the Enigma base alpine system

docker build --build-arg USER_ID=$(id -u) --build-arg GROUP_ID=$(id -g) -f Dockerfile -t duckhp/enigmabbs .
