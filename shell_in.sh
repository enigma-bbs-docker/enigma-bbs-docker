#!/usr/bin/env bash

# This launches a (bash) shell on a running container
# 
# USAGE:
# 	- run 'docker ps' to obtain desired container id
#	- run './shell_in.sh <container id>' to shell into it
#

#DOCKER_CONTAINER_ID=$1

DOCKER_CONTAINER_ID=$(docker ps | grep "enigmabbs" |  cut -f1 -d ' ')

docker exec -it $DOCKER_CONTAINER_ID /bin/bash
