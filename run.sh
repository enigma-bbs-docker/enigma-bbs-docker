#!/usr/bin/env bash

# Start the docker (and builds enigma bbs to ./enigma-bbs/ if not existing)

ENIGMA_TELNET_PORT=8888
ENIGMA_SSH_PORT=8889

docker run -it -v $(pwd)/enigma-bbs:/home/bbs/enigma-bbs -p $ENIGMA_TELNET_PORT:8888 -p $ENIGMA_SSH_PORT:8889 duckhp/enigmabbs
